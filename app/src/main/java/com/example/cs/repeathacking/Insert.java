package com.example.cs.repeathacking;

import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class Insert extends AppCompatActivity {
    TextView title;
    ImageView photo;
    TextView p1,p2,p3,p4,p5;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert);
        title = (TextView) findViewById(R.id.text1);
        title.setText("The Hare and the Tortoise");
        photo = (ImageView) findViewById(R.id.photo);
        p1=(TextView)findViewById(R.id.p1);
        p2=(TextView)findViewById(R.id.p2);
        p3=(TextView)findViewById(R.id.p3);
        p4=(TextView)findViewById(R.id.p4);
        p5=(TextView)findViewById(R.id.p5);
        // photo.setVisibility(View.INVISIBLE);
        //photo.setImageBitmap(employee_One.getBitmap());
        //story = (TextView) findViewById(R.id.name);
        // story.setText("" + employee_One.getAge());

//        empname.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                empphoto.setVisibility(View.VISIBLE);
//            }
//        });

        SpannableString ss = new SpannableString("There once was a speedy Hare who bragged about how fast he could run. Tired of hearing him boast, the Tortoise challenged him to a race. All the animals in the forest gathered to watch.\n");
        SpannableString ss2 =new SpannableString("The Hare ran down the road for a while and then paused to rest. He looked back at the tortoise and cried out, \"How do you expect to win this race when you are walking along at your slow, slow pace?" );
        SpannableString ss3= new SpannableString("The Hare stretched himself out alongside the road and fell asleep, thinking, \"There is plenty of time to relax." );
        SpannableString ss4= new SpannableString("The Tortoise walked and walked, never ever stopping until he came to the finish line.");
        SpannableString ss5= new SpannableString("The animals who were watching cheered so loudly for Tortoise that they woke up the Hare. The Hare stretched, yawned and began to run again, but it was too late. Tortoise had already crossed the finish line. The tortoise won the race.");

        //final ImagePopup imagePopup = new ImagePopup(this);
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View p1) {
                // startActivity(new Intent(InsertandRetrieveBlobData.this, NextActivity.class));
                // photo.setVisibility(View.VISIBLE);
                //      imagePopup.initiatePopup(photo.getDrawable());
                //Bitmap icon = BitmapFactory.decodeResource(getResources(), R.drawable.jan);
                photo.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.hare));
                // float imageWidthInPX = (float)photo.getWidth();

                //LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(Math.round(imageWidthInPX),
                //      Math.round(imageWidthInPX * (float)photo.getHeight() / (float)photo.getWidth()));
                //photo.setLayoutParams(layoutParams);

            }
            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };
        ClickableSpan clickableSpan2= new ClickableSpan() {
            @Override
            public void onClick(View p2) {
                photo.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.tortoise));
            }
        };

        ClickableSpan clickableSpan3= new ClickableSpan() {
            @Override
            public void onClick(View p3) {
                photo.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.road));
            }
        };

        ClickableSpan clickableSpan4= new ClickableSpan() {
            @Override
            public void onClick(View p4) {
                photo.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.finish));
            }
        };
        ClickableSpan clickableSpan5= new ClickableSpan() {
            @Override
            public void onClick(View p5) {
                photo.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.end));
            }
        };

        ss.setSpan(clickableSpan, 24, 28, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss2.setSpan(clickableSpan2, 86, 94, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss3.setSpan(clickableSpan3, 45, 50, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss4.setSpan(clickableSpan4, 73, 84, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss5.setSpan(clickableSpan5, 220, 223, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        p1.setText(ss);
        p1.setMovementMethod(LinkMovementMethod.getInstance());
        p1.setHighlightColor(Color.YELLOW);

        p2.setText(ss2);
        p2.setMovementMethod(LinkMovementMethod.getInstance());
        p2.setHighlightColor(Color.YELLOW) ;

        p3.setText(ss3);
        p3.setMovementMethod(LinkMovementMethod.getInstance());
        p3.setHighlightColor(Color.YELLOW) ;

        p4.setText(ss4);
        p4.setMovementMethod(LinkMovementMethod.getInstance());
        p4.setHighlightColor(Color.YELLOW) ;

        p5.setText(ss5);
        p5.setMovementMethod(LinkMovementMethod.getInstance());
        p5.setHighlightColor(Color.YELLOW) ;

    }
}
